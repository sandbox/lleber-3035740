<?php

namespace Drupal\Tests\cloudhooks\Unit\Composer;

use Composer\Composer;
use Composer\Config;
use Composer\Installer\InstallationManager;
use Composer\IO\IOInterface;
use Drupal\cloudhooks\Composer\CloudhookPlugin;
use Drupal\cloudhooks\Composer\Installer\CloudhookInstallerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class CloudhookPluginTest.
 *
 * @covers \Drupal\cloudhooks\Composer\CloudhookPlugin
 *
 * @package \Drupal\cloudhooks\Tests\Unit
 */
class CloudhookPluginTest extends TestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\cloudhooks\Composer\CloudhookPluginInterface
   */
  protected $cloudhookPlugin;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->cloudhookPlugin = new CloudhookPlugin();
  }

  /**
   * Test case for the ::getInstaller method.
   *
   * @covers \Drupal\cloudhooks\Composer\CloudhookPlugin::getInstaller
   *
   * @throws \ReflectionException
   *   This should never happen.
   */
  public function testGetInstaller() {

    /* @var $config_mock \Composer\Config|\PHPUnit\Framework\MockObject\MockObject */
    $config_mock = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $composer_mock \Composer\Composer|\PHPUnit\Framework\MockObject\MockObject */
    $composer_mock = $this->getMockBuilder(Composer::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $io_mock \Composer\IO\IOInterface|\PHPUnit\Framework\MockObject\MockObject */
    $io_mock = $this->getMockBuilder(IOInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $composer_mock
      ->method('getConfig')
      ->willReturn($config_mock);

    $installer = $this->cloudhookPlugin->getInstaller($composer_mock, $io_mock);

    static::assertInstanceOf(CloudhookInstallerInterface::class, $installer);
  }

  /**
   * Test case for the ::activate method.
   *
   * @covers \Drupal\cloudhooks\Composer\CloudhookPlugin::activate
   *
   * @throws \ReflectionException
   *   This should never happen.
   */
  public function testActivate() {

    /* @var $config_mock \Composer\Config|\PHPUnit\Framework\MockObject\MockObject */
    $config_mock = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $composer_mock \Composer\Composer|\PHPUnit\Framework\MockObject\MockObject */
    $composer_mock = $this->getMockBuilder(Composer::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $io_mock \Composer\IO\IOInterface|\PHPUnit\Framework\MockObject\MockObject */
    $io_mock = $this->getMockBuilder(IOInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $install_mgr_mock \Composer\Installer\InstallationManager|\PHPUnit\Framework\MockObject\MockObject */
    $install_mgr_mock = $this->getMockBuilder(InstallationManager::class)
      ->disableOriginalConstructor()
      ->getMock();

    $composer_mock
      ->expects(static::once())
      ->method('getInstallationManager')
      ->willReturn($install_mgr_mock);

    $composer_mock
      ->method('getConfig')
      ->willReturn($config_mock);

    $install_mgr_mock->expects(static::once())
      ->method('addInstaller');

    $this->cloudhookPlugin->activate($composer_mock, $io_mock);
  }

}
