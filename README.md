# Cloudhook Installer Plugin

This is a composer plugin that allows the cloudhooks bootstrap files to be installed with composer.

## Unit Tests
This project utilizes <a href="https://travis-ci.org">Travis CI</a> for continuous integration testing and <a href="https://codecov.io">Codecov</a> for displaying coverage reports and history.

[![Build Status](https://travis-ci.org/LukeLeber/cloudhook-installer-plugin.svg?branch=master)](https://travis-ci.org/LukeLeber/cloudhook-installer-plugin)
[![codecov](https://codecov.io/gh/LukeLeber/cloudhook-installer-plugin/branch/master/graph/badge.svg)](https://codecov.io/gh/LukeLeber/cloudhook-installer-plugin)

## Code Quality
This project utilizes <a href="https://codeclimate.com/">Codeclimate</a> for generating and sharing code quality metrics.

[![Maintainability](https://api.codeclimate.com/v1/badges/4702a0d52b539b149b57/maintainability)](https://codeclimate.com/github/LukeLeber/cloudhook-installer-plugin/maintainability)

## Documentation
This project utilizes <a href="https://readthedocs.org">Read the Docs</a> for documentation building and hosting.

[![Documentation Status](https://readthedocs.org/projects/cloudhook-installer-plugin/badge/?version=latest)](https://cloudhook-installer-plugin.readthedocs.io/en/latest/?badge=latest)

## Contributing
This project leverages <a href="https://stickler-ci.com">Stickler CI</a> to enforce the Drupal coding standard.  All pull requests will be subject to automated review before being passed off to a human.