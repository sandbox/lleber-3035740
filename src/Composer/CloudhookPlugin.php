<?php

namespace Drupal\cloudhooks\Composer;

use Composer\Composer;
use Composer\Installer\LibraryInstaller;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;

/**
 * A composer plugin type that adds introduces a cloudhook installer.
 *
 * @package lleber\Composer
 */
class CloudhookPlugin extends LibraryInstaller {

  /**
   * {@inheritdoc}
   */
  public function getInstallPath(PackageInterface $package) {
    return 'hooks';
  }

  /**
   * {@inheritdoc}
   */
  public function supports($packageType) {
    return 'drupal-cloudhooks' === $packageType;
  }

  /**
   * {@inheritdoc}
   */
  public function activate(Composer $composer, IOInterface $io) {
    $this->composer = $composer;
    $this->io = $io;
    $composer->getInstallationManager()->addInstaller($this);
  }

}
