<?php

namespace Drupal\Tests\cloudhooks\Unit\Composer\Installer;

use Composer\Composer;
use Composer\Config;
use Composer\Installer\BinaryInstaller;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Drupal\cloudhooks\Composer\Installer\CloudhookInstaller;
use PHPUnit\Framework\TestCase;

/**
 * Class CloudhookInstallerTest.
 *
 * @package Drupal\Tests\cloudhooks\Unit\Composer\Installer
 */
class CloudhookInstallerTest extends TestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\cloudhooks\Composer\Installer\CloudhookInstallerInterface
   */
  protected $installer;

  /**
   * {@inheritdoc}
   *
   * @throws \ReflectionException
   *   This should never happen.
   */
  protected function setUp(): void {
    parent::setUp();

    /* @var $config_mock \Composer\Config|\PHPUnit\Framework\MockObject\MockObject */
    $config_mock = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $composer_mock \Composer\Composer|\PHPUnit\Framework\MockObject\MockObject */
    $composer_mock = $this->getMockBuilder(Composer::class)
      ->disableOriginalConstructor()
      ->getMock();

    $composer_mock
      ->method('getConfig')
      ->willReturn($config_mock);

    /* @var $io_mock \Composer\IO\IOInterface|\PHPUnit\Framework\MockObject\MockObject */
    $io_mock = $this->getMockBuilder(IOInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    /* @var $binary_installer_mock \Composer\Installer\BinaryInstaller|\PHPUnit\Framework\MockObject\MockObject */
    $binary_installer_mock = $this->getMockBuilder(BinaryInstaller::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->installer = new CloudhookInstaller($io_mock, $composer_mock, 'library', NULL, $binary_installer_mock);

  }

  /**
   * Test case for the ::getInstallPath method.
   *
   * @covers \Drupal\cloudhooks\Composer\Installer\CloudhookInstaller::getInstallPath
   *
   * @throws \ReflectionException
   *   This should never happen.
   */
  public function testGetInstallPath() {

    /* @var $package_mock \Composer\Package\PackageInterface|\PHPUnit\Framework\MockObject\MockObject */
    $package_mock = $this->getMockBuilder(PackageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    static::assertEquals('hooks', $this->installer->getInstallPath($package_mock));
  }

  /**
   * Test case for the ::supports method.
   *
   * @param array $data
   *   The sample data passed in from the provider.
   *
   * @covers \Drupal\cloudhooks\Composer\Installer\CloudhookInstaller::supports
   *
   * @dataProvider supportsProvider
   */
  public function testSupports(array $data) {
    $actual = $this->installer->supports($data['package']);
    static::assertEquals($data['expected'], $actual);
  }

  /**
   * Data provider for ::testSupports.
   *
   * @return array
   *   Sample data.
   */
  public function supportsProvider() {
    return [
      'true' => [
        [
          'expected' => TRUE,
          'package' => 'drupal-cloudhooks',
        ],
      ],
      'false' => [
        [
          'expected' => FALSE,
          'package' => 'not-drupal-cloudhooks',
        ],
      ],
    ];
  }

}
